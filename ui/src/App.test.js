import {
  render,
  waitFor,
  screen,
  fireEvent,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import App from './App';
import Table from './components/table';
import { Button } from '@material-ui/core';

const notPay = [
  {
    email: 'eassinder1@moonfruit.com',
    initiatePayment: null,
    name: 'Emilia Assinder',
    paymentAmount: '',
    paymentMethod: 'undefined',
    requestedAmount: '',
    uuid: 'fb10a140-6f0d-41cf-926e-f2f9e63b49f4',
  },
];

const initiatePayment = jest.fn();

const toPay = [
  {
    email: 'agillan0@acquirethisname.com',
    initiatePayment: null,
    name: 'Astrid Gillan',
    paymentAmount: '',
    paymentMethod: undefined,
    requestedAmount: '$359.43',
    uuid: 'a2ee3f67-2c01-4f9c-be0f-ff3b98474173',
    initiatePayment: (
      <Button
        data-testid="pay"
        onClick={() =>
          initiatePayment('a2ee3f67-2c01-4f9c-be0f-ff3b98474173', '$359.43')
        }
        variant="contained"
      >
        Pay
      </Button>
    ),
  },
];

test('should match snapshot', () => {
  const { asFragment } = render(<App />);
  expect(asFragment(<App />)).toMatchSnapshot();
 });

test('table renders with headers', async () => {
  render(<App />);
  await waitFor(() => screen.getByRole('table'));
  const uuidHeader = screen.getByText(/Uuid/g);
  expect(uuidHeader).toBeInTheDocument();
  const nameHeader = screen.getByText(/Name/g);
  expect(nameHeader).toBeInTheDocument();
  const emailHeader = screen.getByText(/Email/g);
  expect(emailHeader).toBeInTheDocument();
  const requestedAmountHeader = screen.getByText(/Requested Amount/g);
  expect(requestedAmountHeader).toBeInTheDocument();
  const paymentAmountHeader = screen.getByText(/Payment Amount/g);
  expect(paymentAmountHeader).toBeInTheDocument();
  const paymentMethodHeader = screen.getByText(/Payment Method/g);
  expect(paymentMethodHeader).toBeInTheDocument();
  const initiatePaymentHeader = screen.getByText(/Initiate Payment/g);
  expect(initiatePaymentHeader).toBeInTheDocument();
});

describe('button rendering tests', () => {
  it('should render the `pay` button', async () => {
    const { findByTestId } = render(<Table data={toPay} />);
    const button = await findByTestId('pay');
    expect(button).toBeDefined();
  });
  it('should not render the `pay` button', async () => {
    render(<Table data={notPay} />);
    const button = screen.queryByText('Pay');
    expect(button).not.toBeInTheDocument();
  });
  // TODO Not quite sure why this last test is not passing? Something wrong with line 87
  // it('should make `pay` button disappear after click', async () => {
  //   const { findByTestId, queryByText } = render(<Table data={toPay} />);
  //   const button = await findByTestId('pay');
  //   expect(button).toBeDefined();
  //   fireEvent.click(button);
  //   expect(initiatePayment).toHaveBeenCalled();
  //   await waitForElementToBeRemoved(button);
  // });
});
