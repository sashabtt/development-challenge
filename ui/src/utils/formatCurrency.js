const formatCurrency = (value) => {
  if (value)
    return `${(value / 100).toLocaleString("en-US", {
      style: "currency",
      currency: "USD",
    })}`;
  return "";
};

export default formatCurrency;

// linter was complaining that an arrow function needed to be assigned to a variable before exporting as default
// so I updated this function to do that.