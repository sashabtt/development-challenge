
# First Challenge
Welcome adventurer...

A student success manager (customer support) is having some trouble with the dashboard. They tell you that they clicked the pay button for `Kay Parsisson` and it didn't work. When the student success manager clicks the pay button for them it pays many more students than just them! 😱 Please fix the issue so that the student success manager can keep issuing payments confidently.

<!-- JUST NOTES FOR MYSELF TO KEEP MY THOUGHTS ORGANIZED  -->
<!-- Kay Parsisson does not have a requested amount to be paid, clicking the pay button also "pays" out many other students but those other students also have $0 amounts requested. So the solution to fix this would be to make sure the Button is only available to students with a requested amount on their account!  -->